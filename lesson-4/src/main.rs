// 自身mod
#![feature(with_options)]
mod file_editing;

use num_cpus;
use std::fmt::Display;
use structopt::StructOpt;
/// 一个命令行工具
#[derive(StructOpt, Debug)]
#[structopt(name = "basic")]
struct Opt {
    /// Activate debug mode
    #[structopt(short, long)]
    debug: bool,
    /// 可输入 c | m | d ,分别代表 CPU 内存 磁盘
    #[structopt(short, long)]
    cmds: Option<String>,
    /// 文件名默认当前路径
    #[structopt(short, long)]
    file: Option<String>,
    /// 被替换字符串
    #[structopt(short, long)]
    old: Option<String>,
    /// 替换字符串
    #[structopt(short, long)]
    new: Option<String>,
}

impl Opt {
    fn new() -> Self {
        Opt {
            debug: false,
            cmds: Some("cpu".into()),
            file: Some("README.md".into()),
            old: Some("1".into()),
            new: Some("2".into()),
        }
    }
}
impl Display for Opt {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "-----Opt Start-----\ncmd: {:?}\nold: {:?}\nnew:{:?}\n------Opt end------",
            self.cmds, self.old, self.new
        )
    }
}
fn main() {
    //let opt = Opt::new();
    let opt = Opt::from_args();
    let cmds = opt.cmds;
    let filePath = opt.file;
    let new = opt.new;
    let old = opt.old;
    for cmd in cmds {
        match cmd.as_str() {
            "cpu" => {
                println!(
                    "-----CPU-----\nPhysical：{}\nLogical   {}\n-----CPU-----",
                    num_cpus::get_physical(),
                    num_cpus::get()
                );
            }
            "memory" => {
                println!("-----Temporarily unavailable-----");
            }
            "disk" => println!("-----Temporarily unavailable-----"),
            _ => {}
        }
    }
    if let Some(file_name) = filePath {
        let path = String::from("./") + file_name.as_str();
        let mut o = String::new();
        match old {
            Some(o0) => {
                o = o0;
                print!("> {} > 替换为 ", o)
            }
            None => {
                //file_editing::println(path.as_str());
            }
        }
        match new {
            Some(n) => {
                print!("> {} <", n);
                match file_editing::replace(path.as_str(), o.as_str(), n.as_str()) {
                    Ok(_) => {}
                    Err(e) => println!("{}", e),
                }
            }
            None => {
                file_editing::println(path.as_str());
            }
        }
    }

    println!()
}
