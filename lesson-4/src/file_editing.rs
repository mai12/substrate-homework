use std::fs::File;
use std::io::prelude::*;

pub fn println(path: &str) {
    let mut f = File::open(path).unwrap();
    let mut buf = String::new();
    f.read_to_string(&mut buf).unwrap();
    println!("{}", buf);
}

pub fn replace(file: &str, old: &str, new: &str) -> std::io::Result<()> {
    // let mut f = File::open(file).unwrap();
    let mut f = File::with_options().read(true).write(true).open(file)?;
    let mut buf = String::new();
    f.read_to_string(&mut buf).unwrap();
    std::fs::remove_file(file).expect("could not remove file");
    let mut f = File::create(file)?;
    let buf = buf.replace(old, new);
    match f.write(buf.as_bytes()) {
        Ok(_) => {}
        Err(e) => println!("{:?}", e),
    };
    Ok(())
    //println!("{}", buf);
}
